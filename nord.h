#define nord0  "#2E3440"
#define nord1  "#3B4252"
#define nord2  "#434C5E"
#define nord3  "#4C566A"
#define nord4  "#D8DEE9"
#define nord5  "#E5E9F0"
#define nord6  "#ECEFF4"
#define nord7  "#8FBCBB"
#define nord8  "#88C0D0"
#define nord9  "#81A1C1"
#define nord10 "#5E81AC"
#define nord11 "#BF616A"
#define nord12 "#D08770"
#define nord13 "#EBCB8B"
#define nord14 "#A3BE8C"
#define nord15 "#B48EAD"

const char *colorname[] = {

  /* 8 normal colors */
  [0] = nord1, /* black   */
  [1] = nord11, /* red     */
  [2] = nord14, /* green   */
  [3] = nord13, /* yellow  */
  [4] = nord9, /* blue    */
  [5] = nord15, /* magenta */
  [6] = nord8, /* cyan    */
  [7] = nord5, /* white   */

  /* 8 bright colors */
  [8]  = nord3,  /* black   */
  [9]  = nord11,  /* red     */
  [10] = nord14, /* green   */
  [11] = nord13, /* yellow  */
  [12] = nord9, /* blue    */
  [13] = nord15, /* magenta */
  [14] = nord7, /* cyan    */
  [15] = nord6, /* white   */

  /* special colors */
  [256] = nord0, /* background */
  [257] = nord4, /* foreground */
  [258] = nord4,     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
unsigned int defaultbg = 256;
unsigned int defaultfg = 257;
unsigned int defaultcs = 258;
