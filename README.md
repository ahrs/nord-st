# nord-st

## Usage

* Remove or comment out the default colourscheme in your `config.h`
* Add the following:

```c
#include "/path/to/nord-st/nord.h"
```
